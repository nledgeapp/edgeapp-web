var path = require('path');

module.exports = {
  entry: './app/client/client.js',
  output: {
    path: __dirname + '/build/js',
    filename: 'client.js',
    publicPath: '/build/js/'
  },
  module: {
    loaders: [
      {
        test: /\.jsx$/,
          exclude: /node_modules/,
          loaders: ["babel"]
      },
      {
        test: /\.js$/,
          exclude: /node_modules/,
          loaders: ["babel"]
      }
    ]
  },
  plugins: [],
  resolve: {
    modulesDirectories: [
      'node_modules',
      'app'
    ]
  }
};

