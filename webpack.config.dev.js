var DefinePlugin = require('webpack').DefinePlugin;
var webpackConfig = require('./webpack.config.common');

var plugins = [
  new DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('development')
    }
  })
];

webpackConfig.devtool = 'inline-source-map';
webpackConfig.noParse = [];

webpackConfig.plugins = webpackConfig.plugins.concat(plugins);

module.exports = webpackConfig;
