FROM node:6.2.2

RUN useradd -ms /bin/bash --home /lib node

COPY app /lib/app
COPY stylesheets /lib/stylesheets
COPY package.json Gruntfile.js .babelrc .nvmrc /lib/
COPY webpack.*.js /lib/

RUN chown -R node:node /lib

USER node
WORKDIR /lib

#ENV NODE_ENV production
RUN npm install -q
RUN npm rebuild node-sass
