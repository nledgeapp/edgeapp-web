# EdgeApp

Main application for entire EdgeApp16 service.

Dependencies to service: `edgeapp`

### Prerequisites:

Make sure you have the correct node version.

- Install nvm (Node Version Manager)
    - Install for Mac: `brew install nvm`
    - Install for Windows: (follow guide here: https://github.com/coreybutler/nvm-windows)
- Run: `nvm install`

### Installation

```
npm install
```

To run:

```
npm start
```