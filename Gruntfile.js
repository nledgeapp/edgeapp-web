module.exports = function(grunt) {

  var config = {
    imgSrc: 'app/public/img',
    imgDest: 'build/img'
  };


  grunt.initConfig({
    config: config,
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      options: {
        outputStyle: 'expanded'
      },
      dev: {
        files: {
          './build/stylesheets/main.css': './stylesheets/main.scss'
        }
      },
      dist: {
        files: {
          './build/stylesheets/main.css': './stylesheets/main.scss'
        },
        options: {
          outputStyle: 'compressed'
        }
      }
    },
    copy: {
      img: {
        expand: true,
        cwd: '<%= config.imgSrc %>',
        dest: '<%= config.imgDest %>',
        src: ['**/*']
      }
    },
    watch: {
      options: {
        atBegin: true
      },
      img: {
        files: '<%= config.imgSrc %>/**/*.{jpg, jpeg, png, gif, svg}',
        tasks: ['copy:img']
      },
      css: {
        files: 'stylesheets/**/*.scss',
        tasks: ['sass:dev']
      }
    }
  });
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');


  grunt.registerTask('default', ['watch']);
};
