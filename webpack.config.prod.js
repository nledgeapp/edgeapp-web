
var DefinePlugin = require('webpack').DefinePlugin,
  optimize = require('webpack').optimize;

var webpackConfig = require('./webpack.config.common');

var plugins = [
  new DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production')
    }
  }),
  new optimize.DedupePlugin(),
  new optimize.OccurenceOrderPlugin(true),
  new optimize.UglifyJsPlugin({
    compress: {
      warnings: true
    }
  }),
  new optimize.DedupePlugin()
];
webpackConfig.devtool = 'cheap-module-source-map';
webpackConfig.noParse = [];

webpackConfig.plugins = webpackConfig.plugins.concat(plugins);

module.exports = webpackConfig;
