'use strict';

import React, { Component } from 'react';
import { Link } from 'react-router';

class Menu extends Component {

  render() {
    let { user } = this.props;

    return (
      <div className="nav">
        <div className="container">
          <Link to={`/`}><img src="/img/logo.png"/></Link>
          <ul className="inline-content">
            <li className="inline-content"><Link to={`/profile`}>PROFILE</Link></li>
            <li className="inline-content"><Link to={`/forms`}>FORMS</Link></li>
          </ul>
        </div>
      </div>
    );
  }
}


export default Menu;
