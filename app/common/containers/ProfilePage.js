'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { changeName, addAttribute, removeAttribute } from '../actions/actions';

class ProfilePage extends Component {

  onChangeName(event) {
    let { changeName } = this.props;
    var inputName = event.currentTarget.value;

    changeName({
      name: inputName
    });
  }

  onAddAttribtue(event) {
    if(event.keyCode === 13) {
      let { addAttribute } = this.props;
      var inputName = event.currentTarget.value;

      console.log('AddAttribute: ' + inputName);
      addAttribute({
        attribute: inputName
      });
      this.addAddressNode.value = '';
    }
  }

  onRemoveAttribute(index, attribute) {
    let { removeAttribute } = this.props;
    console.log('removeAttribute: ' + index + ' - ' + attribute);
    removeAttribute({
      index: index
    });
  }

  render() {
    let { user } = this.props;

    return (
      <div className="page profile-page">
        <div className="page-header">
          <div className="container">
            <img className="profile-img inline-content left-content" src={user.img} />
            <div className="profile-info inline-content right-content">
              <p className="profile-title">{user.name}</p>
              <p className="profile-subtitle">{user.assignment}</p>
              <ul className="profile-status">
                <li className="inline-content">&#x2605; 15 Matches</li>
                <li className="inline-content">&#x2630; 4/10 Forms</li>
              </ul>
            </div>
          </div>
        </div>
        <div className="page-content">
          
          <div className="container">
            <div className="page-section">
            {
              user.attributes.map((attribute, index) => {
                return (
                  <p key={'address-' + index}>
                    <button onClick={this.onRemoveAttribute.bind(this, index, attribute)} >X</button>
                    <span>{`${index + 1}: ${attribute}`}</span>
                  </p>
                );
              })
            }
            </div>
            <div className="page-section">
              <h2>Attributes</h2>
              <input ref={(node) => this.addAddressNode = node} type="text" placeholder="Add new attribute" onKeyUp={this.onAddAttribtue.bind(this)} />
              <h1>Profile Page: {user.name}</h1>
              <input type="text" placeholder="Change Name" onChange={this.onChangeName.bind(this)} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeName: (payload) => {
      dispatch(changeName(payload));
    },
    addAttribute: (payload) => {
      dispatch(addAttribute(payload));
    },
    removeAttribute: (payload) => {
      dispatch(removeAttribute(payload))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
