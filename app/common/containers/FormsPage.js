'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

class FormsPage extends Component {
  render() {
    let { user } = this.props;

    return (
      <div className="container">
        <h1>Forms page</h1>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(FormsPage);
