'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import Menu from '../components/Menu';

class App extends Component {
  
  render() {
    let { user, children } = this.props;

    // if(!user) {
    //   return (
    //     <LoginPage />
    //   );
    // }

    return (
      <div>
        <Menu user={user} />
        { React.cloneElement(children, { user }) }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
