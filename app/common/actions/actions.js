'use strict';

export const CHANGE_NAME = 'CHANGE_NAME';
export function changeName(payload) {
  return {
    type: CHANGE_NAME,
    payload
  };
}

export const LOGIN_USER = 'LOGIN_USER';
export function loginUser(user) {
  return {
    type: LOGIN_USER,
    user
  };
}

export const ADD_ATTRIBUTE = 'ADD_ATTRIBUTE';
export function addAttribute(payload) {
  return {
    type: ADD_ATTRIBUTE,
    payload
  };
}

export const REMOVE_ATTRIBUTE = 'REMOVE_ATTRIBUTE';
export function removeAttribute(payload) {
  return {
    type: REMOVE_ATTRIBUTE,
    payload
  };
}
