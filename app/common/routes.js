import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import ProfilePage from './containers/ProfilePage';
import FormsPage from './containers/FormsPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute name="index" component={ProfilePage} />
    <Route path="/profile" name="profile" component={ProfilePage} />
    <Route path="/forms" name="forms" component={FormsPage} />
  </Route>
);
