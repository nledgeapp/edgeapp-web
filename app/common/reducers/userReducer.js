'use strict';

import * as ACTIONS from '../actions/actions';

const INITIAL_USER = {
  name: 'Sara Frisk',
  img: '/img/sara.jpg',
  age: 20,
  consultancyLevel: 'SrC',
  assignment: 'Frontend developer at NetEnt',
  attributes: []
};

const attributes = (state = [], action) => {
  switch(action.type) {
    case ACTIONS.ADD_ATTRIBUTE: {
      return [
        ...state,
        action.payload.attribute
      ];
    }
    case ACTIONS.REMOVE_ATTRIBUTE: {
      let { index } = action.payload;
      return [
        ...state.slice(0, index),
        ...state.slice(index + 1)
      ];
    }
    default: {
      return state;
    }
  }
};

const user = (state = INITIAL_USER, action) => {
  switch(action.type) {
    case ACTIONS.LOGIN_USER: {
      let { user } = action;
      if(user) {
        return {
          name: user.name,
          age: user.age,
          attributes: user.attributes
        };
      }
      return state;
    }
    case ACTIONS.CHANGE_NAME: {
      return {
        ...state,
        name: action.payload.name
      };
    }
    case ACTIONS.REMOVE_ATTRIBUTE:
    case ACTIONS.ADD_ATTRIBUTE: {
      return {
        ...state,
        attributes: attributes(state.attributes, action)
      }
    }
    default: {
      return state;
    }
  }
};

export default user;
