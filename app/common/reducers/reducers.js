'use strict';

import { combineReducers } from 'redux';

import user from './userReducer';

const edgeApp = combineReducers({
  user
});

export default edgeApp;
