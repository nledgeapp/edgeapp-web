'use strict';

import path from 'path';

const config = {
  ROOT: path.join(__dirname, '../../'),
  NODE_ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 3001
};

export default config;
