import express from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import compression from 'compression';

import config from './config';
import pug from 'pug';
import path from 'path';

import { setupMiddlewares } from './middlewares/middlewares';

var app = express();

var morganLogger;
if (config.NODE_ENV === 'development') {
  morganLogger = morgan('dev');
} else {
  /* Log only errors in production */
  morganLogger = morgan('combined', {
    skip: (req, res) => res.statusCode < 400
  });
}
app.use(morganLogger);
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('views', 'app/server/views');
app.engine('jade', pug.__express);
app.set('view engine', 'jade');

app.use(express.static(path.join(config.ROOT, 'build')));

// Routes
setupMiddlewares(app);

// not found route
app.use((req, res, next) => {
  res.status(404).json({
    status: 404,
    message: 'Url not found',
    url: req.url
  });
});

var server = app.listen(config.PORT, () => {
  var host = server.address().address;
  var port = server.address().port;

  console.log(`Server listening at http://${host}:${port}`);
});
