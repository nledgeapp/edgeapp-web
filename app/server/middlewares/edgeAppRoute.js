'use strict';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import { match, RouterContext } from 'react-router';

import { loginUser } from '../../common/actions/actions';
import routes from '../../common/routes';
import edgeApp from '../../common/reducers/reducers';

function rootRoute(req, res, next) {

  const store = createStore(edgeApp);

  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).json({
        status: 500,
        message: error.message
      });
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps) {
      //TODO: Always try to login user!
      store.dispatch(loginUser(req.user));
      
      const markup = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );

      const initialState = JSON.stringify(store.getState());

      res.render('index', {
        markup,
        initialState
      });
    } else {
      next();
    }
  });
}

export default rootRoute;
