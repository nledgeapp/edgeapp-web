'use strict';

import userAuthentication from './userAuthentication';

import edgeAppRoute from './edgeAppRoute';
import loginRoute from './loginRoute';

export function setupMiddlewares(server) {
  server.use(userAuthentication);

  server.get('/login/user', loginRoute);

  server.use(edgeAppRoute);
}
