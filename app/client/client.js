'use strict';

import React from 'react';
import { render } from 'react-dom';

import { browserHistory, Router } from 'react-router';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import edgeApp from '../common/reducers/reducers';
import routes from '../common/routes';
(function() {
  const initialState = window.__INITIAL_STATE__;
  const store = createStore(edgeApp, initialState);

  console.log('InitialState: ', store.getState());
  render(
    <Provider store={store}>
      <Router history={browserHistory} >
        {routes}
      </Router>
    </Provider>,
    document.getElementById('content')
  );

}());
